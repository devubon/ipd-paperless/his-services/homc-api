import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import _ from 'lodash';
import { DateTime } from 'luxon';

import { ServiceModels } from '../../models/services/services_lookup';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db = fastify.db;
  const serviceModels = new ServiceModels();

  fastify.get('/bed', {
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const data: any = await serviceModels.bed(db);
      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data:data
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });


  //ออกแล้ว
  fastify.get('/department', {
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const data: any = await serviceModels.department(db);
      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data:data
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });

  fastify.get('/food', {
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const data: any = await serviceModels.food(db);
      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data:data
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });

  //ออกแล้ว
  fastify.get('/insurance', {
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const data: any = await serviceModels.insurance(db);
      let _data: any = [];
      for (const i of data) {
        let _reference_code = JSON.parse(i.reference_code);
        
        delete i.reference_code;
        i.reference_code = _reference_code;

        _data.push(i);
      }
      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data:_data
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });

  fastify.get('/items', {
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const data: any = await serviceModels.items(db);
      let _data: any = [];
      for (const i of data) {
        let _reference_code = JSON.parse(i.reference_code);
        
        delete i.reference_code;
        i.reference_code = _reference_code;

        let _price = JSON.parse(i.price);
        delete i.price;
        i.price = _price;
        _data.push(i);
      }
      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data:_data
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });

  //ออกแล้ว
  fastify.get('/ward', {
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const data: any = await serviceModels.ward(db);
      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data:data
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });
  
  fastify.get('/medicine', {
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const data: any = await serviceModels.medicine(db);
      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data:data
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });

  fastify.get('/medicine-usage', {
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const data: any = await serviceModels.medicine_usage(db);
      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data:data
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });

  fastify.get('/refer-cause', {
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const data: any = await serviceModels.refer_cause(db);
      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data:data
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });

  fastify.get('/refer-by', {
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const data: any = await serviceModels.refer_by(db);
      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data:data
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });

  fastify.get('/specialist', {
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const data: any = await serviceModels.specialist(db);
      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data:data
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });

  done();

} 
