import S from 'fluent-json-schema'

const querySchema = S.object()
  .prop('an', S.string().maxLength(15).required())
export default {
    querystring: querySchema
}