import { Knex } from 'knex';
export class ServiceModels {
  items(db: Knex) {
    let sql:any = db.raw(`
    SELECT DISTINCT
    b.charge_code as 'code',
    b.charge_des as 'name',
    case 
    when bm.muadCode = 'A' then '10'  
    when bm.muadCode = 'B' then '11'  
    when bm.muadCode = 'C' then '12'  
    when bm.muadCode = 'D' then '13'  
    when bm.muadCode = 'E' then '14'  
    when bm.muadCode = 'F' then '15'  
    when bm.muadCode = 'G' then '16'  
    else bm.muadCode end as 'item_type_id',
    i.ward_id as 'reference_code_code',
    w.ward_name as 'reference_code_value',
    b.charge_code as 'price_code',
    b.amount as 'price_value',
    b.serviceUnit as 'unit'
    
    FROM Ipd_h i
    LEFT JOIN PATDIAG pd on (pd.Hn = i.hn and pd.regNo = i.regist_flag)
    LEFT JOIN Bill_d b on (b.hn = i.hn and b.regist_flag = i.regist_flag )
    LEFT JOIN DOCC c on (c.docCode = pd.DocCode)
    LEFT JOIN Chr_code cc on (cc.chr_code = b.charge_code)
    LEFT JOIN BillMuad bm on (bm.muadCode = cc.BillMuad)
    LEFT JOIN Gn_price gn on (gn.chargeCode = b.charge_code)
    LEFT JOIN Bill_h b1 on (b1.hn = i.hn and b1.regNo = i.regist_flag)
    LEFT JOIN sitt si on (si.SittHOMC = b1.useDrg)
    LEFT JOIN Ward w on (w.ward_id = i.ward_id)
    WHERE i.admit_date BETWEEN @Datebegin and @DateEnd
    and i.hn = @hn and i.regist_flag = @vn
    and bm.muadCode = '1'
    
    union
    
    SELECT DISTINCT
    b.charge_code as 'code',
    b.charge_des as 'name',
    case 
    when bm.muadCode = 'A' then '10'  
    when bm.muadCode = 'B' then '11'  
    when bm.muadCode = 'C' then '12'  
    when bm.muadCode = 'D' then '13'  
    when bm.muadCode = 'E' then '14'  
    when bm.muadCode = 'F' then '15'  
    when bm.muadCode = 'G' then '16'  
    else bm.muadCode end as 'item_type_id',
    b.charge_code as 'reference_code_code',
    b.itemCode as 'reference_code_value',
    b.charge_code as 'price_code',
    b.amount as 'price_value',
    b.serviceUnit as 'unit'
    
    FROM Ipd_h i
    LEFT JOIN PATDIAG pd on (pd.Hn = i.hn and pd.regNo = i.regist_flag)
    LEFT JOIN Bill_d b on (b.hn = i.hn and b.regist_flag = i.regist_flag )
    LEFT JOIN Chr_code cc on (cc.chr_code = b.charge_code)
    LEFT JOIN BillMuad bm on (bm.muadCode = cc.BillMuad)
    LEFT JOIN Gn_price gn on (gn.chargeCode = b.charge_code)
    LEFT JOIN Bill_h b1 on (b1.hn = i.hn and b1.regNo = i.regist_flag)
    WHERE i.admit_date BETWEEN @Datebegin and @DateEnd
    and i.hn = @hn and i.regist_flag = @vn
    and bm.muadCode = '2'
    
    union
    
    SELECT DISTINCT
    b.charge_code as 'code',
    b.charge_des as 'name',
    case 
    when bm.muadCode = 'A' then '10'  
    when bm.muadCode = 'B' then '11'  
    when bm.muadCode = 'C' then '12'  
    when bm.muadCode = 'D' then '13'  
    when bm.muadCode = 'E' then '14'  
    when bm.muadCode = 'F' then '15'  
    when bm.muadCode = 'G' then '16'  
    else bm.muadCode end as 'item_type_id',
    'TMT' as 'reference_code_code',
    mi.TMT as 'reference_code_value',
    
    si.CODE as 'price_code',
    b.amount as 'price_value',
    b.serviceUnit as 'unit'
    
    FROM Ipd_h i
    LEFT JOIN PATDIAG pd on (pd.Hn = i.hn and pd.regNo = i.regist_flag)
    LEFT JOIN Bill_d b on (b.hn = i.hn and b.regist_flag = i.regist_flag )
    LEFT JOIN DOCC c on (c.docCode = pd.DocCode)
    LEFT JOIN Chr_code cc on (cc.chr_code = b.charge_code)
    LEFT JOIN BillMuad bm on (bm.muadCode = cc.BillMuad)
    LEFT JOIN Gn_price gn on (gn.chargeCode = b.charge_code)
    LEFT JOIN Med_inv mi on (mi.name = b.charge_des)
    LEFT JOIN Bill_h b1 on (b1.hn = i.hn and b1.regNo = i.regist_flag)
    LEFT JOIN sitt si on (si.SittHOMC = b1.useDrg)
    WHERE i.admit_date BETWEEN @Datebegin and @DateEnd
    and i.hn = @hn and i.regist_flag = @vn
    and bm.muadCode = '3'
    
    union
    
    SELECT DISTINCT
    b.charge_code as 'code',
    b.charge_des as 'name',
    case 
    when bm.muadCode = 'A' then '10'  
    when bm.muadCode = 'B' then '11'  
    when bm.muadCode = 'C' then '12'  
    when bm.muadCode = 'D' then '13'  
    when bm.muadCode = 'E' then '14'  
    when bm.muadCode = 'F' then '15'  
    when bm.muadCode = 'G' then '16'  
    else bm.muadCode end as 'item_type_id',
    b.charge_code as 'reference_code_code',
    b.itemCode as 'reference_code_value',
    b.charge_code as 'price_code',
    b.amount as 'price_value',
    b.serviceUnit as 'unit'
    
    FROM Ipd_h i
    LEFT JOIN PATDIAG pd on (pd.Hn = i.hn and pd.regNo = i.regist_flag)
    LEFT JOIN Bill_d b on (b.hn = i.hn and b.regist_flag = i.regist_flag )
    LEFT JOIN Chr_code cc on (cc.chr_code = b.charge_code)
    LEFT JOIN BillMuad bm on (bm.muadCode = cc.BillMuad)
    LEFT JOIN Gn_price gn on (gn.chargeCode = b.charge_code)
    LEFT JOIN Bill_h b1 on (b1.hn = i.hn and b1.regNo = i.regist_flag)
    WHERE i.admit_date BETWEEN @Datebegin and @DateEnd
    and i.hn = @hn and i.regist_flag = @vn
    and bm.muadCode = '4'
    
    UNION
    
    SELECT DISTINCT
    b.charge_code as 'code',
    b.charge_des as 'name',
    case 
    when bm.muadCode = 'A' then '10'  
    when bm.muadCode = 'B' then '11'  
    when bm.muadCode = 'C' then '12'  
    when bm.muadCode = 'D' then '13'  
    when bm.muadCode = 'E' then '14'  
    when bm.muadCode = 'F' then '15'  
    when bm.muadCode = 'G' then '16'  
    else bm.muadCode end as 'item_type_id',
    b.charge_code as 'reference_code_code',
    b.itemCode as 'reference_code_value',
    b.charge_code as 'price_code',
    b.amount as 'price_value',
    b.serviceUnit as 'unit'
    
    FROM Ipd_h i
    LEFT JOIN PATDIAG pd on (pd.Hn = i.hn and pd.regNo = i.regist_flag)
    LEFT JOIN Bill_d b on (b.hn = i.hn and b.regist_flag = i.regist_flag )
    LEFT JOIN Chr_code cc on (cc.chr_code = b.charge_code)
    LEFT JOIN BillMuad bm on (bm.muadCode = cc.BillMuad)
    LEFT JOIN Gn_price gn on (gn.chargeCode = b.charge_code)
    LEFT JOIN Bill_h b1 on (b1.hn = i.hn and b1.regNo = i.regist_flag)
    WHERE i.admit_date BETWEEN @Datebegin and @DateEnd
    and i.hn = @hn and i.regist_flag = @vn
    and bm.muadCode = '5'
    
    union
    
    SELECT DISTINCT
    b.charge_code as 'code',
    b.charge_des as 'name',
    case 
    when bm.muadCode = 'A' then '10'  
    when bm.muadCode = 'B' then '11'  
    when bm.muadCode = 'C' then '12'  
    when bm.muadCode = 'D' then '13'  
    when bm.muadCode = 'E' then '14'  
    when bm.muadCode = 'F' then '15'  
    when bm.muadCode = 'G' then '16'  
    else bm.muadCode end as 'item_type_id',
    b.charge_code as 'reference_code_code',
    b.itemCode as 'reference_code_value',
    b.charge_code as 'price_code',
    b.amount as 'price_value',
    b.serviceUnit as 'unit'
    
    FROM Ipd_h i
    LEFT JOIN PATDIAG pd on (pd.Hn = i.hn and pd.regNo = i.regist_flag)
    LEFT JOIN Bill_d b on (b.hn = i.hn and b.regist_flag = i.regist_flag )
    LEFT JOIN Chr_code cc on (cc.chr_code = b.charge_code)
    LEFT JOIN BillMuad bm on (bm.muadCode = cc.BillMuad)
    LEFT JOIN Gn_price gn on (gn.chargeCode = b.charge_code)
    LEFT JOIN Bill_h b1 on (b1.hn = i.hn and b1.regNo = i.regist_flag)
    WHERE i.admit_date BETWEEN @Datebegin and @DateEnd
    and i.hn = @hn and i.regist_flag = @vn
    and bm.muadCode = '6'
    
    union
    
    SELECT DISTINCT
    b.charge_code as 'code',
    b.charge_des as 'name',
    case 
    when bm.muadCode = 'A' then '10'  
    when bm.muadCode = 'B' then '11'  
    when bm.muadCode = 'C' then '12'  
    when bm.muadCode = 'D' then '13'  
    when bm.muadCode = 'E' then '14'  
    when bm.muadCode = 'F' then '15'  
    when bm.muadCode = 'G' then '16'  
    else bm.muadCode end as 'item_type_id',
    'LGO' as 'reference_code_code',
    l1.LGOCode as 'reference_code_value',
    b.charge_code as 'price_code',
    b.amount as 'price_value',
    b.serviceUnit as 'unit'
    
    FROM Ipd_h i
    LEFT JOIN PATDIAG pd on (pd.Hn = i.hn and pd.regNo = i.regist_flag)
    LEFT JOIN Bill_d b on (b.hn = i.hn and b.regist_flag = i.regist_flag )
    LEFT JOIN Chr_code cc on (cc.chr_code = b.charge_code)
    LEFT JOIN BillMuad bm on (bm.muadCode = cc.BillMuad)
    LEFT JOIN Gn_price gn on (gn.chargeCode = b.charge_code)
    LEFT JOIN Bill_h b1 on (b1.hn = i.hn and b1.regNo = i.regist_flag)
    LEFT JOIN Labres_d l on (l.hn = i.hn and l.reg_flag = i.regist_flag)
    LEFT JOIN Lab l1 on (l1.labCode = l.lab_code)
    WHERE i.admit_date BETWEEN @Datebegin and @DateEnd
    and i.hn = @hn and i.regist_flag = @vn
    and bm.muadCode = '7'
    
    union
    
    SELECT DISTINCT
    b.charge_code as 'code',
    b.charge_des as 'name',
    case 
    when bm.muadCode = 'A' then '10'  
    when bm.muadCode = 'B' then '11'  
    when bm.muadCode = 'C' then '12'  
    when bm.muadCode = 'D' then '13'  
    when bm.muadCode = 'E' then '14'  
    when bm.muadCode = 'F' then '15'  
    when bm.muadCode = 'G' then '16'  
    else bm.muadCode end as 'item_type_id',
    b.charge_code as 'reference_code_code',
    b.itemCode as 'reference_code_value',
    b.charge_code as 'price_code',
    b.amount as 'price_value',
    b.serviceUnit as 'unit'
    
    FROM Ipd_h i
    LEFT JOIN PATDIAG pd on (pd.Hn = i.hn and pd.regNo = i.regist_flag)
    LEFT JOIN Bill_d b on (b.hn = i.hn and b.regist_flag = i.regist_flag )
    LEFT JOIN Chr_code cc on (cc.chr_code = b.charge_code)
    LEFT JOIN BillMuad bm on (bm.muadCode = cc.BillMuad)
    LEFT JOIN Gn_price gn on (gn.chargeCode = b.charge_code)
    LEFT JOIN Bill_h b1 on (b1.hn = i.hn and b1.regNo = i.regist_flag)
    WHERE i.admit_date BETWEEN @Datebegin and @DateEnd
    and i.hn = @hn and i.regist_flag = @vn
    and bm.muadCode = '8'
    
    union
    
    SELECT DISTINCT
    b.charge_code as 'code',
    b.charge_des as 'name',
    case 
    when bm.muadCode = 'A' then '10'  
    when bm.muadCode = 'B' then '11'  
    when bm.muadCode = 'C' then '12'  
    when bm.muadCode = 'D' then '13'  
    when bm.muadCode = 'E' then '14'  
    when bm.muadCode = 'F' then '15'  
    when bm.muadCode = 'G' then '16'  
    else bm.muadCode end as 'item_type_id',
    b.charge_code as 'reference_code_code',
    b.itemCode as 'reference_code_value',
    b.charge_code as 'price_code',
    b.amount as 'price_value',
    b.serviceUnit as 'unit'
    
    FROM Ipd_h i
    LEFT JOIN PATDIAG pd on (pd.Hn = i.hn and pd.regNo = i.regist_flag)
    LEFT JOIN Bill_d b on (b.hn = i.hn and b.regist_flag = i.regist_flag )
    LEFT JOIN Chr_code cc on (cc.chr_code = b.charge_code)
    LEFT JOIN BillMuad bm on (bm.muadCode = cc.BillMuad)
    LEFT JOIN Gn_price gn on (gn.chargeCode = b.charge_code)
    LEFT JOIN Bill_h b1 on (b1.hn = i.hn and b1.regNo = i.regist_flag)
    WHERE i.admit_date BETWEEN @Datebegin and @DateEnd
    and i.hn = @hn and i.regist_flag = @vn
    and bm.muadCode = '9'
    
    union
    
    SELECT DISTINCT
    b.charge_code as 'code',
    b.charge_des as 'name',
    case 
    when bm.muadCode = 'A' then '10'  
    when bm.muadCode = 'B' then '11'  
    when bm.muadCode = 'C' then '12'  
    when bm.muadCode = 'D' then '13'  
    when bm.muadCode = 'E' then '14'  
    when bm.muadCode = 'F' then '15'  
    when bm.muadCode = 'G' then '16'  
    else bm.muadCode end as 'item_type_id',
    b.charge_code as 'reference_code_code',
    b.itemCode as 'reference_code_value',
    b.charge_code as 'price_code',
    b.amount as 'price_value',
    b.serviceUnit as 'unit'
    
    FROM Ipd_h i
    LEFT JOIN PATDIAG pd on (pd.Hn = i.hn and pd.regNo = i.regist_flag)
    LEFT JOIN Bill_d b on (b.hn = i.hn and b.regist_flag = i.regist_flag )
    LEFT JOIN Chr_code cc on (cc.chr_code = b.charge_code)
    LEFT JOIN BillMuad bm on (bm.muadCode = cc.BillMuad)
    LEFT JOIN Gn_price gn on (gn.chargeCode = b.charge_code)
    LEFT JOIN Bill_h b1 on (b1.hn = i.hn and b1.regNo = i.regist_flag)
    WHERE i.admit_date BETWEEN @Datebegin and @DateEnd
    and i.hn = @hn and i.regist_flag = @vn
    and bm.muadCode = 'A'
    
    union
    
    SELECT DISTINCT
    b.charge_code as 'code',
    b.charge_des as 'name',
    case 
    when bm.muadCode = 'A' then '10'  
    when bm.muadCode = 'B' then '11'  
    when bm.muadCode = 'C' then '12'  
    when bm.muadCode = 'D' then '13'  
    when bm.muadCode = 'E' then '14'  
    when bm.muadCode = 'F' then '15'  
    when bm.muadCode = 'G' then '16'  
    else bm.muadCode end as 'item_type_id',
    b.charge_code as 'reference_code_code',
    b.itemCode as 'reference_code_value',
    b.charge_code as 'price_code',
    b.amount as 'price_value',
    b.serviceUnit as 'unit'
    
    FROM Ipd_h i
    LEFT JOIN PATDIAG pd on (pd.Hn = i.hn and pd.regNo = i.regist_flag)
    LEFT JOIN Bill_d b on (b.hn = i.hn and b.regist_flag = i.regist_flag )
    LEFT JOIN Chr_code cc on (cc.chr_code = b.charge_code)
    LEFT JOIN BillMuad bm on (bm.muadCode = cc.BillMuad)
    LEFT JOIN Gn_price gn on (gn.chargeCode = b.charge_code)
    LEFT JOIN Bill_h b1 on (b1.hn = i.hn and b1.regNo = i.regist_flag)
    WHERE i.admit_date BETWEEN @Datebegin and @DateEnd
    and i.hn = @hn and i.regist_flag = @vn
    and bm.muadCode = 'A'
    
    union
    
    SELECT DISTINCT
    b.charge_code as 'code',
    b.charge_des as 'name',
    case 
    when bm.muadCode = 'A' then '10'  
    when bm.muadCode = 'B' then '11'  
    when bm.muadCode = 'C' then '12'  
    when bm.muadCode = 'D' then '13'  
    when bm.muadCode = 'E' then '14'  
    when bm.muadCode = 'F' then '15'  
    when bm.muadCode = 'G' then '16'  
    else bm.muadCode end as 'item_type_id',
    b.charge_code as 'reference_code_code',
    b.itemCode as 'reference_code_value',
    b.charge_code as 'price_code',
    b.amount as 'price_value',
    b.serviceUnit as 'unit'
    
    FROM Ipd_h i
    LEFT JOIN PATDIAG pd on (pd.Hn = i.hn and pd.regNo = i.regist_flag)
    LEFT JOIN Bill_d b on (b.hn = i.hn and b.regist_flag = i.regist_flag )
    LEFT JOIN Chr_code cc on (cc.chr_code = b.charge_code)
    LEFT JOIN BillMuad bm on (bm.muadCode = cc.BillMuad)
    LEFT JOIN Gn_price gn on (gn.chargeCode = b.charge_code)
    LEFT JOIN Bill_h b1 on (b1.hn = i.hn and b1.regNo = i.regist_flag)
    WHERE i.admit_date BETWEEN @Datebegin and @DateEnd
    and i.hn = @hn and i.regist_flag = @vn
    and bm.muadCode = 'B'
    
    union
    
    SELECT DISTINCT
    b.charge_code as 'code',
    b.charge_des as 'name',
    case 
    when bm.muadCode = 'A' then '10'  
    when bm.muadCode = 'B' then '11'  
    when bm.muadCode = 'C' then '12'  
    when bm.muadCode = 'D' then '13'  
    when bm.muadCode = 'E' then '14'  
    when bm.muadCode = 'F' then '15'  
    when bm.muadCode = 'G' then '16'  
    else bm.muadCode end as 'item_type_id',
    b.charge_code as 'reference_code_code',
    b.itemCode as 'reference_code_value',
    b.charge_code as 'price_code',
    b.amount as 'price_value',
    b.serviceUnit as 'unit'
    
    FROM Ipd_h i
    LEFT JOIN PATDIAG pd on (pd.Hn = i.hn and pd.regNo = i.regist_flag)
    LEFT JOIN Bill_d b on (b.hn = i.hn and b.regist_flag = i.regist_flag )
    LEFT JOIN Chr_code cc on (cc.chr_code = b.charge_code)
    LEFT JOIN BillMuad bm on (bm.muadCode = cc.BillMuad)
    LEFT JOIN Gn_price gn on (gn.chargeCode = b.charge_code)
    LEFT JOIN Bill_h b1 on (b1.hn = i.hn and b1.regNo = i.regist_flag)
    WHERE i.admit_date BETWEEN @Datebegin and @DateEnd
    and i.hn = @hn and i.regist_flag = @vn
    and bm.muadCode = 'C'
    
    union
    
    SELECT DISTINCT
    b.charge_code as 'code',
    b.charge_des as 'name',
    case 
    when bm.muadCode = 'A' then '10'  
    when bm.muadCode = 'B' then '11'  
    when bm.muadCode = 'C' then '12'  
    when bm.muadCode = 'D' then '13'  
    when bm.muadCode = 'E' then '14'  
    when bm.muadCode = 'F' then '15'  
    when bm.muadCode = 'G' then '16'  
    else bm.muadCode end as 'item_type_id',
    b.charge_code as 'reference_code_code',
    b.itemCode as 'reference_code_value',
    b.charge_code as 'price_code',
    b.amount as 'price_value',
    b.serviceUnit as 'unit'
    
    FROM Ipd_h i
    LEFT JOIN PATDIAG pd on (pd.Hn = i.hn and pd.regNo = i.regist_flag)
    LEFT JOIN Bill_d b on (b.hn = i.hn and b.regist_flag = i.regist_flag )
    LEFT JOIN Chr_code cc on (cc.chr_code = b.charge_code)
    LEFT JOIN BillMuad bm on (bm.muadCode = cc.BillMuad)
    LEFT JOIN Gn_price gn on (gn.chargeCode = b.charge_code)
    LEFT JOIN Bill_h b1 on (b1.hn = i.hn and b1.regNo = i.regist_flag)
    WHERE i.admit_date BETWEEN @Datebegin and @DateEnd
    and i.hn = @hn and i.regist_flag = @vn
    and bm.muadCode = 'D'
    
    union
    
    SELECT DISTINCT
    b.charge_code as 'code',
    b.charge_des as 'name',
    case 
    when bm.muadCode = 'A' then '10'  
    when bm.muadCode = 'B' then '11'  
    when bm.muadCode = 'C' then '12'  
    when bm.muadCode = 'D' then '13'  
    when bm.muadCode = 'E' then '14'  
    when bm.muadCode = 'F' then '15'  
    when bm.muadCode = 'G' then '16'  
    else bm.muadCode end as 'item_type_id',
    b.charge_code as 'reference_code_code',
    b.itemCode as 'reference_code_value',
    b.charge_code as 'price_code',
    b.amount as 'price_value',
    b.serviceUnit as 'unit'
    
    FROM Ipd_h i
    LEFT JOIN PATDIAG pd on (pd.Hn = i.hn and pd.regNo = i.regist_flag)
    LEFT JOIN Bill_d b on (b.hn = i.hn and b.regist_flag = i.regist_flag )
    LEFT JOIN Chr_code cc on (cc.chr_code = b.charge_code)
    LEFT JOIN BillMuad bm on (bm.muadCode = cc.BillMuad)
    LEFT JOIN Gn_price gn on (gn.chargeCode = b.charge_code)
    LEFT JOIN Bill_h b1 on (b1.hn = i.hn and b1.regNo = i.regist_flag)
    WHERE i.admit_date BETWEEN @Datebegin and @DateEnd
    and i.hn = @hn and i.regist_flag = @vn
    and bm.muadCode = 'E'
    
    union 
    
    SELECT DISTINCT
    b.charge_code as 'code',
    b.charge_des as 'name',
    case 
    when bm.muadCode = 'A' then '10'  
    when bm.muadCode = 'B' then '11'  
    when bm.muadCode = 'C' then '12'  
    when bm.muadCode = 'D' then '13'  
    when bm.muadCode = 'E' then '14'  
    when bm.muadCode = 'F' then '15'  
    when bm.muadCode = 'G' then '16'  
    else bm.muadCode end as 'item_type_id',
    b.charge_code as 'reference_code_code',
    b.itemCode as 'reference_code_value',
    b.charge_code as 'price_code',
    b.amount as 'price_value',
    b.serviceUnit as 'unit'
    
    FROM Ipd_h i
    LEFT JOIN PATDIAG pd on (pd.Hn = i.hn and pd.regNo = i.regist_flag)
    LEFT JOIN Bill_d b on (b.hn = i.hn and b.regist_flag = i.regist_flag )
    LEFT JOIN Chr_code cc on (cc.chr_code = b.charge_code)
    LEFT JOIN BillMuad bm on (bm.muadCode = cc.BillMuad)
    LEFT JOIN Gn_price gn on (gn.chargeCode = b.charge_code)
    LEFT JOIN Bill_h b1 on (b1.hn = i.hn and b1.regNo = i.regist_flag)
    WHERE i.admit_date BETWEEN @Datebegin and @DateEnd
    and i.hn = @hn and i.regist_flag = @vn
    and bm.muadCode = 'F'
    
    union
    
    SELECT DISTINCT
    b.charge_code as 'code',
    b.charge_des as 'name',
    case 
    when bm.muadCode = 'A' then '10'  
    when bm.muadCode = 'B' then '11'  
    when bm.muadCode = 'C' then '12'  
    when bm.muadCode = 'D' then '13'  
    when bm.muadCode = 'E' then '14'  
    when bm.muadCode = 'F' then '15'  
    when bm.muadCode = 'G' then '16'  
    else bm.muadCode end as 'item_type_id',
    b.charge_code as 'reference_code_code',
    b.itemCode as 'reference_code_value',
    b.charge_code as 'price_code',
    b.amount as 'price_value',
    b.serviceUnit as 'unit'
    
    FROM Ipd_h i
    LEFT JOIN PATDIAG pd on (pd.Hn = i.hn and pd.regNo = i.regist_flag)
    LEFT JOIN Bill_d b on (b.hn = i.hn and b.regist_flag = i.regist_flag )
    LEFT JOIN Chr_code cc on (cc.chr_code = b.charge_code)
    LEFT JOIN BillMuad bm on (bm.muadCode = cc.BillMuad)
    LEFT JOIN Gn_price gn on (gn.chargeCode = b.charge_code)
    LEFT JOIN Bill_h b1 on (b1.hn = i.hn and b1.regNo = i.regist_flag)
    WHERE i.admit_date BETWEEN @Datebegin and @DateEnd
    and i.hn = @hn and i.regist_flag = @vn
    and bm.muadCode = 'G'
    
    union
    
    SELECT DISTINCT
    b.charge_code as 'code',
    b.charge_des as 'name',
    case 
    when bm.muadCode = 'A' then '10'  
    when bm.muadCode = 'B' then '11'  
    when bm.muadCode = 'C' then '12'  
    when bm.muadCode = 'D' then '13'  
    when bm.muadCode = 'E' then '14'  
    when bm.muadCode = 'F' then '15'  
    when bm.muadCode = 'G' then '16'  
    else bm.muadCode end as 'item_type_id',
    b.charge_code as 'reference_code_code',
    b.itemCode as 'reference_code_value',
    b.charge_code as 'price_code',
    b.amount as 'price_value',
    b.serviceUnit as 'unit'
    
    FROM Ipd_h i
    LEFT JOIN PATDIAG pd on (pd.Hn = i.hn and pd.regNo = i.regist_flag)
    LEFT JOIN Bill_d b on (b.hn = i.hn and b.regist_flag = i.regist_flag )
    LEFT JOIN Chr_code cc on (cc.chr_code = b.charge_code)
    LEFT JOIN BillMuad bm on (bm.muadCode = cc.BillMuad)
    LEFT JOIN Gn_price gn on (gn.chargeCode = b.charge_code)
    LEFT JOIN Bill_h b1 on (b1.hn = i.hn and b1.regNo = i.regist_flag)
    WHERE i.admit_date BETWEEN @Datebegin and @DateEnd
    and i.hn = @hn and i.regist_flag = @vn
    and bm.muadCode = '17'
    
    `);    
    return sql;
  }

  insurance(db: Knex) {
    let sql:any = db.raw(`
    SELECT DISTINCT
    b.useDrg as 'code',
    p.pay_typedes as 'name',
    'INSCL' as 'code',
    su.SUBINSCL as 'value'
    FROM Ipd_h i
    LEFT JOIN Bill_h b on (b.hn = i.hn and b.regNo = i.regist_flag)
    LEFT JOIN Paytype p on (p.pay_typecode = b.useDrg)
    LEFT JOIN sitt si on (si.SittHOMC = b.useDrg)
    LEFT JOIN SittUtil su on (su.HOMCHN = i.hn and su.REGNO = i.regist_flag) 
    WHERE b.useDrg <> ''
    
    union
    
    SELECT DISTINCT
    b.useDrg as 'code',
    p.pay_typedes as 'name',
    'Eclaim' as 'code',
    b.useDrg as 'value'
    FROM Ipd_h i
    LEFT JOIN Bill_h b on (b.hn = i.hn and b.regNo = i.regist_flag)
    LEFT JOIN Paytype p on (p.pay_typecode = b.useDrg)
    WHERE b.useDrg <> ''
    
    union
    
    SELECT DISTINCT
    b.useDrg as 'code',
    p.pay_typedes as 'name',
    'AIPN' as 'code',
    b.useDrg as 'value'
    FROM Ipd_h i
    LEFT JOIN Bill_h b on (b.hn = i.hn and b.regNo = i.regist_flag)
    LEFT JOIN Paytype p on (p.pay_typecode = b.useDrg)
    WHERE b.useDrg <> ''
    `);    
    return sql;
  }

  ward(db: Knex) {
    let sql:any = db.raw(`
    SELECT DISTINCT
    w.ward_id as 'code',
    w.ward_name as 'Ward',
    w.qId as 'department_id',
    null as 'bed_amount'
    FROM Ipd_h i
    LEFT JOIN Resident r on (r.hn = i.hn and r.regist_flag = i.regist_flag)
    LEFT JOIN Ward w on (w.ward_id = i.ward_id)
    WHERE i.admit_date <> ''
    `);    
    return sql;
  }

  department(db: Knex) {
    let sql:any = db.raw(`
    SELECT 
    DISTINCT 
    w.qId as 'code',
    q.QDESC as 'name'
    FROM Ipd_h i
    LEFT JOIN Ward w on (w.ward_id = i.ward_id)
    LEFT JOIN QID q on (q.QID = w.qId)
    WHERE i.ward_id <> ''
    `);    
    return sql;
  }

  food(db: Knex) {
    let sql:any = db.raw(``);    
    return sql;
  }

  bed(db: Knex) {
    let sql:any = db.raw(`
    ELECT DISTINCT
    w.ward_name as 'name',
    null as 'standard_price',
    null as 'extra_paid'

    FROM Ipd_h i 
    LEFT JOIN Ward w on (w.ward_id = i.ward_id)
    WHERE i.admit_date <> ''
    `);    
    return sql;
  }

  medicine(db: Knex) {
    let sql:any = db.raw(`
    SELECT DISTINCT 
    mi.code as 'code',
    mi.name as 'name',
    concat(rtrim(mi.strgth),mi.strgth_u) as 'strength',
    mi.package as 'unit',
    mi.packsize as 'product_cat',
    mi.code as 'default_usage',
    mi.dform as 'default_route',
    'true' as 'is_ed',
    mi.name as 'tallman',
    case 
    when bm.muadCode = 'A' then '10'  
    when bm.muadCode = 'B' then '11'  
    when bm.muadCode = 'C' then '12'  
    when bm.muadCode = 'D' then '13'  
    when bm.muadCode = 'E' then '14'  
    when bm.muadCode = 'F' then '15'  
    when bm.muadCode = 'G' then '16'  
    else bm.muadCode end as 'medicine_type_id',
    null as 'is_prenant_allow'
    FROM Ipd_h i
    LEFT JOIN Patmed pm on (pm.hn = i.hn and pm.registNo = i.regist_flag)
    LEFT JOIN Med_inv mi on (mi.code = pm.invCode)
    LEFT JOIN Bill_d b on (b.hn = i.hn and b.regist_flag = i.regist_flag)
    LEFT JOIN Chr_code cc on (cc.chr_code = b.charge_code)
    LEFT JOIN BillMuad bm on (bm.muadCode = cc.BillMuad)
    WHERE  bm.muadCode = '3'
    `);    
    return sql;
  }

  medicine_usage(db: Knex) {
    let sql:any = db.raw(`
    SELECT DISTINCT 
    pm.invCode as 'code',
    
    RTRIM(ISNULL(CASE when pm.lamedEng='N' then h.lamed_name else h.lamed_eng end,''))+' '+
    dbo.doseText(CASE when RTRIM(ISNULL(pm.lamedQty,''))<>'' and RTRIM(ISNULL(pm.lamedQty,''))<>'0' then RTRIM(ISNULL(pm.lamedQty,''))
    ELSE RTRIM(ISNULL(pm.qtyPerFeed,'')) end,RTRIM(ISNULL( CASE when pm.lamedEng='N' then u.lamed_name else u.lamed_eng end,'')),u.translate,isnull( pm.lamedEng,'N'))
    +' '+RTRIM(ISNULL(pm.lamedTimeText,''))+' '+RTRIM(ISNULL(pm.lamedText,'')) as 'description',
    
    pm.periodPerDay as 'time_per_day'
    FROM Ipd_h i
    LEFT JOIN Patmed pm on (pm.hn = i.hn and pm.registNo = i.regist_flag)
    LEFT JOIN Med_inv mi on (mi.code = pm.invCode)
    LEFT JOIN Lamed h (NOLOCK) on ('*'+pm.lamedHow= h.lamed_code or pm.lamedHow= h.lamed_code)
    LEFT JOIN Lamed u (NOLOCK) on ('!' + pm.lamedUnit= u.lamed_code or pm.lamedUnit= u.lamed_code)
    LEFT JOIN Lamed t (NOLOCK) on ('&' + pm.lamedTime= t.lamed_code or pm.lamedTime= t.lamed_code)
    LEFT JOIN Lamed s (NOLOCK) on ('@' + pm.lamedSpecial= s.lamed_code or pm.lamedSpecial= s.lamed_code)
    LEFT JOIN Bill_d b on (b.hn = i.hn and b.regist_flag = i.regist_flag)
    LEFT JOIN Chr_code cc on (cc.chr_code = b.charge_code)
    LEFT JOIN BillMuad bm on (bm.muadCode = cc.BillMuad)
    WHERE  bm.muadCode = '3'
    `);    
    return sql;
  }

  refer_cause(db: Knex) {
    let sql:any = db.raw(`
    SELECT DISTINCT 
    r.ReferReason as 'code',
    rs.REASON_DESC as 'name' 
    FROM Ipd_h i 
    LEFT JOIN Refer r on (r.Hn = i.hn and r.RegNo = i.regist_flag)
    LEFT JOIN REFERRS rs on (rs.REASON_CODE = r.ReferReason)
    WHERE   r.ReferStatus = 'O'
    `);    
    return sql;
  }

  refer_by(db: Knex) {
    let sql:any = db.raw(`
    SELECT DISTINCT
    sr.loads_id as 'code',
    sr.loads_name as 'name'
    FROM Ipd_h i
    LEFT JOIN Refer r on (r.Hn = i.hn and r.RegNo = i.regist_flag)
    LEFT JOIN smartrefer_sps sr on (sr.hn = ltrim(r.Hn) and dbo.ce2ymd(sr.refer_date) = r.ReferDate )
    LEFT JOIN Refer_by rb on (rb.loads_id = sr.loads_id)
    WHERE i.admit_date <> ''
    `);    
    return sql;
  }

  specialist(db: Knex) {
    let sql:any = db.raw(`
    SELECT DISTINCT
    QID as 'code',
    QDESC as 'name'
    FROM Ipd_h i
    LEFT JOIN Ward w on (w.ward_id = i.ward_id)
    LEFT JOIN QID q on (q.QID = w.qId)
    WHERE i.admit_date <> ''
    `);    
    return sql;
  }
 
}