import { log } from 'console';
import { Knex } from 'knex';
export class ServiceModels {
  /**
   * บันทึกข้อมูล Admit
   * @param db 
   * @param data 
   * @returns 
   */

/**
 * ข้อมูลทั่วไปผู้รับบริการ HIS 
 * @param db 
 * @param an 
 * @param limit 
 * @param offset 
 * @returns 
 */
  patient(db: Knex,an: string, limit: number, offset: number,dateStart: any,dateEnd: number) {
    if(an){
      let sql:any = db.raw(`
      SELECT 
      i.ladmit_n as 'an',
      i.hn as 'hn',
      s.CardID as 'cid',
      PT.titleName as 'title',
      P.firstName as 'fname',
      P.lastName as 'lname',
      sc.mean as 'gender',
      concat(dbo.ymd2ceDate102(P.birthDay),'T00:00:00.000Z') as 'dob',
      
      concat(abs(datediff(month, dbo.ymd2ce(P.birthDay), dbo.ymd2ce(i.admit_date)))/12,'-', 
      abs(datediff(month, dbo.ymd2ce(P.birthDay), dbo.ymd2ce(i.admit_date)))%12,'-',
      abs(datediff(day, dbo.ymd2ce(P.birthDay), dbo.ymd2ce(i.admit_date)))%12) as 'age', 
      
      n.NATDES as 'nationality',
      n.NATDES as 'citizenship',
      rl.REGDES as 'religion',
      mr.maritalDesc  as 'married_status',
      oc.occdes as 'occupation',
      P.bloodGroup as 'blood_group',
      su.SUBINSCL as 'inscl',
      su.SUBINSCL_NAME as 'inscl_name',
      isnull(
      CONCAT(+LTRIM(RTRIM(P.addr1)),' '+LTRIM(RTRIM(P.addr2)), ' หมู่ที่ '+ LTRIM(RTRIM(dbo.padc(P.moo,'0',1))),' '+LTRIM(RTRIM(T.tambonName)), ' '+LTRIM(RTRIM(R.regionName)), ' '+LTRIM(RTRIM(A.areaName))),i.respadd1) as 'address',
      
      case when i.respphone = '' then P.mobilephone else i.respphone end as 'phone',
      '0' as 'is_pregnant',  --ตั้งครรภ์
      i.respadd1 as 'reference_person_address',
      i.respphone as 'reference_person_phone',
      i.respstat as 'reference_person_relationship',
      concat(dbo.ymd2ceDate102(i.admit_date),'T00:00:00.000Z') as 'admit_date',
      concat(SUBSTRING(i.admit_time,1,2),':',SUBSTRING(i.admit_time,3,2),':00') as 'admit_time',
      ic.DES as 'pre_diag',
      rtrim(c.doctitle)+''+rtrim(c.docName) +'  '+rtrim(c.docLName) as 'admit_by'
      FROM Ipd_h i
      LEFT JOIN PATDIAG pd on (pd.Hn = i.hn and pd.regNo = i.regist_flag and pd.DiagType in ('I','E')  and pd.dxtype ='1')
      LEFT JOIN ICD101 ic on (ic.CODE = pd.ICDCode)
      left join PATIENT P  on (P.hn=i.hn)
      left join PTITLE PT on ( PT.titleCode = P.titleCode)
      left join PatSS s on(P.hn=s.hn)
      left join sexcode sc on (P.sex = sc.code)
      left join  AREA  A on (A.areaCode = P.areaCode)
      left join REGION R on (R.regionCode = P.regionCode)
      left join Nation n on (n.NATCODE=P.nation)
      LEFT JOIN Religion rl on (rl.REGCODE = P.religion)
      left join Tambon T on (T.tambonCode = (P.regionCode + P.tambonCode ))
      left join Marital mr on (P.marital = mr.maritalCode)
      LEFT JOIN Occup oc on (oc.occcode = P.occupation)
      LEFT JOIN DOCC c on (c.docCode = i.doccode)
      LEFT JOIN SittUtil su on (su.HOMCHN = i.hn and su.REGNO = i.regist_flag)
      WHERE dbo.ymd2ceDate102(i.admit_date) BETWEEN ? and ?
      and i.ladmit_n = ?    
      `,[dateStart,dateEnd,an]);    
      return sql;
      }else{
        let sql:any = db.raw(`
        SELECT 
        i.ladmit_n as 'an',
        i.hn as 'hn',
        s.CardID as 'cid',
        PT.titleName as 'title',
        P.firstName as 'fname',
        P.lastName as 'lname',
        sc.mean as 'gender',
        concat(dbo.ymd2ceDate102(P.birthDay),'T00:00:00.000Z') as 'dob',
        
        concat(abs(datediff(month, dbo.ymd2ce(P.birthDay), dbo.ymd2ce(i.admit_date)))/12,'-', 
        abs(datediff(month, dbo.ymd2ce(P.birthDay), dbo.ymd2ce(i.admit_date)))%12,'-',
        abs(datediff(day, dbo.ymd2ce(P.birthDay), dbo.ymd2ce(i.admit_date)))%12) as 'age', 
        
        n.NATDES as 'nationality',
        n.NATDES as 'citizenship',
        rl.REGDES as 'religion',
        mr.maritalDesc  as 'married_status',
        oc.occdes as 'occupation',
        P.bloodGroup as 'blood_group',
        su.SUBINSCL as 'inscl',
        su.SUBINSCL_NAME as 'inscl_name',
        isnull(
        CONCAT(+LTRIM(RTRIM(P.addr1)),' '+LTRIM(RTRIM(P.addr2)), ' หมู่ที่ '+ LTRIM(RTRIM(dbo.padc(P.moo,'0',1))),' '+LTRIM(RTRIM(T.tambonName)), ' '+LTRIM(RTRIM(R.regionName)), ' '+LTRIM(RTRIM(A.areaName))),i.respadd1) as 'address',
        
        case when i.respphone = '' then P.mobilephone else i.respphone end as 'phone',
        '0' as 'is_pregnant',  --ตั้งครรภ์
        i.respadd1 as 'reference_person_address',
        i.respphone as 'reference_person_phone',
        i.respstat as 'reference_person_relationship',
        concat(dbo.ymd2ceDate102(i.admit_date),'T00:00:00.000Z') as 'admit_date',
        concat(SUBSTRING(i.admit_time,1,2),':',SUBSTRING(i.admit_time,3,2),':00') as 'admit_time',
        ic.DES as 'pre_diag',
        rtrim(c.doctitle)+''+rtrim(c.docName) +'  '+rtrim(c.docLName) as 'admit_by'
        FROM Ipd_h i
        LEFT JOIN PATDIAG pd on (pd.Hn = i.hn and pd.regNo = i.regist_flag and pd.DiagType in ('I','E')  and pd.dxtype ='1')
        LEFT JOIN ICD101 ic on (ic.CODE = pd.ICDCode)
        left join PATIENT P  on (P.hn=i.hn)
        left join PTITLE PT on ( PT.titleCode = P.titleCode)
        left join PatSS s on(P.hn=s.hn)
        left join sexcode sc on (P.sex = sc.code)
        left join  AREA  A on (A.areaCode = P.areaCode)
        left join REGION R on (R.regionCode = P.regionCode)
        left join Nation n on (n.NATCODE=P.nation)
        LEFT JOIN Religion rl on (rl.REGCODE = P.religion)
        left join Tambon T on (T.tambonCode = (P.regionCode + P.tambonCode ))
        left join Marital mr on (P.marital = mr.maritalCode)
        LEFT JOIN Occup oc on (oc.occcode = P.occupation)
        LEFT JOIN DOCC c on (c.docCode = i.doccode)
        LEFT JOIN SittUtil su on (su.HOMCHN = i.hn and su.REGNO = i.regist_flag)
        WHERE dbo.ymd2ceDate102(i.admit_date) BETWEEN ? and ?
        `,[dateStart,dateEnd]);    
        return sql;
        }
  }

  review(db: Knex,an: string) {
    let sql:any = db.raw(`
    SELECT DISTINCT
    i.ladmit_n as 'an',
    i.regist_flag as 'vn',
    null as 'chieft_complaint',
    null as 'present_illness',
    null as 'past_history',
    null as 'physical_exam',
    vl.Weight as 'body_weight',
    vl.Height as 'body_height',
    vl.Pulse as 'pulse_rate',
    vl.Breathe as 'respiratory_rate',
    vl.Temperature as 'body_temperature',
    vl.Hbloodpress as 'systolic_blood_pressure',
    vl.Lbloodpress as 'diastolic_blood_pressure',
    vl.O2sat as 'oxygen_sat',
    vl.eye as 'eye_score',
    null as 'movement_score',
    null as 'verbal_score',
    dbo.ymd2ceDate102(o.registDate) as 'visit_date',
    concat(SUBSTRING(o.timePt,1,2),':',SUBSTRING(o.timePt,3,2),':00') as 'visit_time'
    
    FROM Ipd_h i
    LEFT JOIN VitalSign vl on (vl.hn = i.hn and vl.RegNo = i.regist_flag)
    LEFT JOIN OPD_H o on (o.hn = i.hn and o.regNo = i.regist_flag)
    WHERE i.ladmit_n = ? `,[an]);    
    return sql;
  }

  treatement(db: Knex,an: string) {
    let sql:any = db.raw(`
    SELECT DISTINCT
    i.ladmit_n as 'an',
    case 
    when bm.muadCode = 'A' then '10'  
    when bm.muadCode = 'B' then '11'  
    when bm.muadCode = 'C' then '12'  
    when bm.muadCode = 'D' then '13'  
    when bm.muadCode = 'E' then '14'  
    when bm.muadCode = 'F' then '15'  
    when bm.muadCode = 'G' then '16'  
    else bm.muadCode end as 'item_type_id',  
    b.charge_code as 'item_code' ,
    b.charge_des as 'item_name' ,
    bm.muadDes as 'item_describe' ,
    ltrim(b.amount) as 'price' ,
    isnull(ltrim(b.itemQuant),'1') as 'quantity' ,
    dbo.ymd2ceDate102(i.admit_date) as 'treatment_date' ,
    concat(SUBSTRING(i.admit_time,1,2),':','00') as 'treatment_time' ,
    rtrim(c.doctitle)+''+rtrim(c.docName) +'  '+rtrim(c.docLName) as 'treatment_by' 
    FROM Ipd_h i
    LEFT JOIN PATDIAG pd on (pd.Hn = i.hn and pd.regNo = i.regist_flag)
    LEFT JOIN Bill_d b on (b.hn = i.hn and b.regist_flag = i.regist_flag )
    LEFT JOIN DOCC c on (c.docCode = pd.DocCode)
    LEFT JOIN Chr_code cc on (cc.chr_code = b.charge_code)
    LEFT JOIN BillMuad bm on (bm.muadCode = cc.BillMuad)
    WHERE  i.ladmit_n = ? `,[an]);
    return sql;
  }

  admit(db: Knex,an: string) {

    let sql:any = db.raw(`
    SELECT DISTINCT 
    i.ladmit_n as 'an',
    i.hn as 'hn',
    i.regist_flag as 'vn',
    dbo.ymd2ceDate102(i.admit_date) as 'admit_date',
    concat(SUBSTRING(i.admit_time,1,2),':',SUBSTRING(i.admit_time,3,2),':','00') as 'admit_time',
    rtrim(c.doctitle)+''+rtrim(c.docName) +'  '+rtrim(c.docLName) as 'admit_by',
    w.qId as 'department_code',
    w.ward_id as 'ward_code',
    '' as 'bed_code',
    '' as 'bed_number',
    si.INSCL as 'inscl_code',
    t1.pay_typedes as 'inscl_name',
    ic.DES as 'pre_diag'
    FROM Ipd_h i
    LEFT JOIN Ward w on (w.ward_id = i.ward_id )
    LEFT JOIN DOCC c on (c.docCode = i.doccode)
    LEFT JOIN PATDIAG pd on (pd.Hn = i.hn and pd.regNo = i.regist_flag and pd.DiagType in ('I','E') and pd.dxtype ='1' )
    LEFT JOIN Bill_h b on (b.hn = i.hn and b.regNo = i.regist_flag)
    LEFT JOIN Paytype t1 on (b.useDrg=t1.pay_typecode)
    LEFT JOIN ICD101 ic on (ic.CODE = pd.ICDCode)
    LEFT JOIN sitt si on (si.SittHOMC = b.useDrg)
    WHERE 
    i.ladmit_n = ? 
    `,[an]);
    
    
    return sql;
  }  

  lab_result(db: Knex,an: string,items_code:string) {
    let sql:any = db.raw(`
    SELECT DISTINCT
    l.lab_code as 'code',
    l.result_name as 'name',
    l.result_name as 'lab_test_name',
    l.real_res as 'lab_test_result',
    ls.result_unit as 'lab_test_unit',
    concat(rtrim(ls.low_normal),'-',ls.high_normal) as 'lab_test_ref_range',
    dbo.ymd2ceDate102(l.res_date) as 'lab_test_result_date',
    concat(SUBSTRING(l.res_time,1,2),':',SUBSTRING(l.res_time,3,2),':','00') as 'lab_test_result_time',
    null as 'lab_test_result_by'
    FROM Ipd_h i
    LEFT JOIN Labres_d l on (l.hn = i.hn and l.reg_flag = i.regist_flag)
    LEFT JOIN Labre_s ls on (ls.lab_code = l.lab_code)
    WHERE l.real_res <> ''
    and l.real_res <> '-'
    and ls.result_unit <> ''
    and i.ladmit_n = ?  and l.lab_code = ?
    `,[an,items_code]);    
    return sql;
  }

  xray_result(db: Knex,an: string) {
    let sql:any = db.raw(`
    SELECT
    p.xpart_code as 'code',
    p.xpart_des as 'name',
    '' as 'result',
    dbo.ymd2ceDate102(dbo.ce2ymd(xh.xreq_date)) as 'result_date',
    xh.xreq_time as 'result_time',
    xh.usrname as 'result_by'
    
    FROM Ipd_h i
    LEFT JOIN Xreq_h xh on (xh.hn = i.hn and xh.regist_flag = i.regist_flag)
    LEFT JOIN Xpart p ON (p.xprefix = xh.xprefix)
    WHERE i.ladmit_n = ?`,[an]);    
    return sql;
  }
  ekg_file(db: Knex,an: string) {
    let sql:any = db.raw(``,[an]);    
    return sql;
  }  

  patient_allergy(db: Knex,an: string) {
    let sql:any = db.raw(`
    SELECT DISTINCT
    m.hn as 'hn',
    mi.name as 'drug_name',
    m.severity as 'allergy_level',
    m.alergyNote as 'symptom',
    dbo.ymd2ceDate102(dbo.ce2ymd(m.updDate)) as 'allergy_date'
    
    FROM Ipd_h i
    LEFT JOIN medalery m on (m.hn = i.hn)
    LEFT JOIN Med_inv mi on (mi.code = m.medCode)
    WHERE i.ladmit_n = ?
    `,[an]);    
    return sql;
  } 
}